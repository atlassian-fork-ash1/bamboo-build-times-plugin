package com.atlassian.bamboo.plugin.buildtimes;


import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.build.PlanResultsAction;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.chains.cache.ImmutableChainStage;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ViewBuildTimes extends PlanResultsAction {
    private boolean isJob;
    private AgentManager agentManager;
    private BuildExecutionManager buildExecutionManager;
    private long averageBuildTime;
    private long elapsedTime;
    
    public List<JobTime> jobTimes = new ArrayList<JobTime>();
    public List<Integer> stageJobs = new ArrayList<Integer>();

    public String doExecute() throws Exception {
        String result = super.doExecute();
        String buildKey = this.getBuildKey();
        if (buildKey.split("-").length == 3) {
            this.isJob = true;
        }
        else {
            this.isJob = false;
        }

        return result;

    }
    
    public String doJSON() throws Exception {
        String result = super.doExecute();
        ChainResultsSummary chainResultsSummary = getChainResultsSummary();
        jobTimes = getJobTimes(chainResultsSummary);
        averageBuildTime = TimeUnit.MILLISECONDS.toSeconds(chainResultsSummary.getImmutablePlan().getAverageBuildDuration() );
        stageJobs = getStageDivisions();
        elapsedTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - chainResultsSummary.getQueueTime().getTime());
        return result;
    }

    @Override
    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonObject = super.getJsonObject();
        List<JSONObject> jsonTimes;

        jsonTimes = getJSONTimes(jobTimes);
        jsonObject.put("jobs", jsonTimes);
        jsonObject.put("stageJobs", this.stageJobs);
        jsonObject.put("averageBuildTime", this.averageBuildTime);
        jsonObject.put("elapsedTime", this.elapsedTime);

        return jsonObject;
    }
    
    private List<JSONObject> getJSONTimes(List<JobTime> jobTimes) throws JSONException{
        List<JSONObject> result = new ArrayList<JSONObject>();
        for (JobTime jobTime : jobTimes) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.append("jobName", jobTime.getJobName());
            jsonObject.append("duration", jobTime.getVcsUpdatingDuration() + jobTime.getBuildingDuration());
            jsonObject.append("queueTime", jobTime.getQueueDuration());
            jsonObject.append("totalDuration", jobTime.getTotalDuration());
            jsonObject.append("result", jobTime.getResult());
            jsonObject.append("agentName", jobTime.getAgentName());
            jsonObject.append("preQueueTime", jobTime.getPreQueuedDuration());
            jsonObject.append("vcsUpdatingTime", jobTime.getVcsUpdatingDuration());
            jsonObject.append("buildingTime", jobTime.getBuildingDuration());
            jsonObject.append("agentId", jobTime.getAgentId());
            jsonObject.append("rerun", jobTime.isRerun());
            result.add(jsonObject);
        }
        
        return result;
    }

    private List<Integer> getStageDivisions()
    {
        ArrayList<Integer> stageDivisions = new ArrayList<Integer>();
        List<? extends ImmutableChainStage> stages = getChainResultsSummary().getImmutablePlan().getStages();
        for(ImmutableChainStage stage: stages)
        {
            stageDivisions.add(stage.getJobs().size());
        }
        return stageDivisions;
    }
    
    private List<JobTime> getJobTimes(ChainResultsSummary chainResultsSummary) {
        List<JobTime> jobTimes = new ArrayList<JobTime>();
        long maxTime = 0;
        if (chainResultsSummary == null) {

        }
        else {
            List<ResultsSummary> jobResultSummaries = chainResultsSummary.getOrderedJobResultSummaries();
            for (ResultsSummary jobResultSummary : jobResultSummaries) {
                JobTime jobTime = new JobTime((BuildResultsSummary) jobResultSummary, chainResultsSummary.getQueueTime(), agentManager, buildExecutionManager);
                jobTimes.add(jobTime);
                maxTime = Math.max(jobTime.getTotalDuration(), maxTime);
            }
        }
        return jobTimes;
    }

    private ChainResultsSummary getChainResultsSummary() {
        if (resultsSummary instanceof ChainResultsSummary) {
            return (ChainResultsSummary) resultsSummary;
        }
        else {
            return resultsSummaryManager.getParentResultSummary(resultsSummary);
        }
    }
    
    public void setAgentManager(AgentManager agentManager) {
        this.agentManager = agentManager;
    }

    public boolean getIsJob() {
        return this.isJob;
    }

    public long getAverageBuildTime()
    {
        return averageBuildTime;
    }

    public void setBuildExecutionManager(BuildExecutionManager buildExecutionManager)
    {
        this.buildExecutionManager = buildExecutionManager;
    }
}
