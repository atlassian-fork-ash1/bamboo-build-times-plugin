package com.atlassian.bamboo.plugin.buildtimes;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import com.opensymphony.webwork.dispatcher.json.JSONArray;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class ViewBuildTimesTest {
    private ViewBuildTimes viewBuildTimes;

    @Injectable
    private JobTime jobTime;
    @Injectable
    private ChainResultsSummary resultsSummary;
    @Injectable
    private BuildResultsSummary buildResultsSummary;
    @Injectable
    private AgentManager agentManager;
    @Injectable
    private BuildAgent buildAgent;
    @Injectable
    private ImmutableChain immutablePlan;
    @Injectable
    private Date queueTime;

    @BeforeMethod
    public void setUp() throws Exception {
        viewBuildTimes = new ViewBuildTimes();
    }

    private static String getStringFromJSONArray(Object array) throws Exception {
        return ((JSONArray)array).get(0).toString();
    }

    private static long getLongFromJSONArray(Object array) throws Exception {
        return Long.valueOf(((JSONArray)array).get(0).toString());
    }

    @Test
    public void getJSONTimesTest() throws Exception {
        final List<BuildResultsSummary> jobTimes = new ArrayList<BuildResultsSummary>();
        final String jobName = "SOME_JOB";
        final long jobQueue = 10;
        final long jobVcsUpdating = 20;
        final long jobBuilding = 30;
        final long jobTotal = 40;
        final long jobPrequeue = -50;
        final String jobResult = "green";
        final String jobAgent = "SOME_AGENT";
        final long jobAgentId = 60;
        final boolean jobIsRerun = false;
        final List<Integer> stageJobs = new ArrayList<Integer>();

        jobTimes.add(buildResultsSummary);
        viewBuildTimes.setAgentManager(agentManager);

        new NonStrictExpectations() {{
            jobTime.getJobName();
            result = jobName;
            jobTime.getVcsUpdatingDuration();
            result = TimeUnit.SECONDS.toMillis(jobVcsUpdating);
            jobTime.getBuildingDuration();
            result = TimeUnit.SECONDS.toMillis(jobBuilding);
            jobTime.getQueueDuration();
            result = TimeUnit.SECONDS.toMillis(jobQueue);
            jobTime.getTotalDuration();
            result = TimeUnit.SECONDS.toMillis(jobTotal);
            jobTime.getResult();
            result = jobResult;
            jobTime.getAgentName();
            result = jobAgent;
            jobTime.getPreQueuedDuration();
            result = TimeUnit.SECONDS.toMillis(jobPrequeue);
            jobTime.getAgentId();
            result = jobAgentId;
            jobTime.isRerun();
            result = jobIsRerun;

            agentManager.getAgent(jobAgentId);
            result = buildAgent;
            buildAgent.getName();
            result = jobAgent;

            resultsSummary.getImmutablePlan();
            result = immutablePlan;
            resultsSummary.getOrderedJobResultSummaries();
            result = jobTimes;
            resultsSummary.getQueueTime();
            result = queueTime;
            queueTime.getTime();
            result = 0;

            buildResultsSummary.getQueueTime();
            result = queueTime;
            buildResultsSummary.getQueueDuration();
            result = TimeUnit.SECONDS.toMillis(jobQueue);
            buildResultsSummary.getVcsUpdateDuration();
            result = TimeUnit.SECONDS.toMillis(jobVcsUpdating);
            buildResultsSummary.getDuration();
            result = TimeUnit.SECONDS.toMillis(jobBuilding);
            buildResultsSummary.getBuildAgentId();
            result = jobAgentId;
            buildResultsSummary.isRebuild();
            result = jobIsRerun;
            buildResultsSummary.getImmutablePlan();
            result = immutablePlan;

            immutablePlan.getBuildName();
            result = jobName;
            immutablePlan.getAverageBuildDuration();
            result = jobBuilding;

            buildResultsSummary.isFinished();
            result = true;
            buildResultsSummary.isSuccessful();
            result = true;

        }};

        viewBuildTimes.setResultsSummary(resultsSummary);

        viewBuildTimes.doJSON();

        JSONObject jsonObject = viewBuildTimes.getJsonObject();

        assertThat(jsonObject.get("status").toString(), is("OK"));

        JSONObject job = (JSONObject)((JSONArray)(jsonObject.get("jobs"))).get(0);
        assertThat(ViewBuildTimesTest.getStringFromJSONArray(job.get("jobName")), is(jobName));
        assertThat(ViewBuildTimesTest.getLongFromJSONArray(job.get("duration")), is(jobVcsUpdating + jobBuilding));
        assertThat(ViewBuildTimesTest.getLongFromJSONArray(job.get("queueTime")), is(jobQueue));
        assertThat(ViewBuildTimesTest.getLongFromJSONArray(job.get("totalDuration")), is(jobQueue + jobVcsUpdating + jobBuilding));
        assertThat(ViewBuildTimesTest.getStringFromJSONArray(job.get("result")), is(jobResult));
        assertThat(ViewBuildTimesTest.getStringFromJSONArray(job.get("agentName")), is(jobAgent));
        assertThat(ViewBuildTimesTest.getLongFromJSONArray(job.get("agentId")), is(jobAgentId));

}

}
